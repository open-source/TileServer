This is a Java Tileserver based on [Mapsforge](https://github.com/mapsforge/mapsforge) and [Openstreetmap](https://www.openstreetmap.de/). 
 
It can run in one of two modes:
1. Without arguments (Tile Server Mode)
Without arguments this Program will answer to requests to port 63825. The Following commands are possible and have to be ended by a newline:  
{z};{x};{y} - Will create a png for the specified tile and returns the data (binary)  
list-files;minLon;minLat;maxLon;maxLat - Will give a JSON Response which lists informations about the files to download for the given area  
download-files;minLon;minLat;maxLon;maxLat - Will create and send a tar file containing above listed files (mapfiles and pregenerated png tiles)  
download-assets - Will create and send a tar file containing our style.xml and assets for the maps.metager.de style  
updater - Will create a process that takes updates from an instance running in updater mode 
2. Updater Mode
To start this mode supply the jar file with the following four arguments  
Tileserver Url (An Url that points to a running Tileserver instance i.e. localhost:63825)  
Data Url (An Url that points to an *.osm.pbf File to use as datasource i.e. http://download.geofabrik.de/europe/germany/bremen-latest.osm.pbf)  
Poly URL (An Url that points to an .poly File that describes the bounds of the area in the OSM File i.e. http://download.geofabrik.de/europe/germany/bremen.poly)  
Ram in GB (How much Ram the Updater Process is allowed to use. Depending on the area at least 12GB are suggested but you might get away with less ie. 12)  
The Updater will then start the following process
  1. Download *.osm.pbf File
  2. Merge the OSM File with the Land Polygons and Water for that Area
  2. Sort *.osm.pbf File
  3. Split the *.osm.pbf File (Splits the File into a lot of smaller Files that can later be downloaded seperately by the app)
  4. Convert the splitted *.osm.pbf Files to *.map Files
  5. Send the *.map Files to the Tileserver instance
  6. Generate pregenerated Tiles for zoom levels 0-13 (Since it takes too long to generate them on the fly)
  7. Send the pregenerated Tiles to the Tileserver
  Sleep until the next update is due and start again with step 1. (Standard Update interval is 7 days)


Used external projects, sotfware and data:
* [Mapsforge](https://github.com/mapsforge/mapsforge)
* [Openstreetmap](https://www.openstreetmap.de/)
* [Mkgmap Splitter](http://www.mkgmap.org.uk/download/splitter.html)
* [Openstreetmap Data](http://openstreetmapdata.com/data/land-polygons)

The Server works without configuration for our needs. If we find the
time we'll provide the possibility to configure all of this
dynamically.