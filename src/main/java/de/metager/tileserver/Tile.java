package de.metager.tileserver;

import org.mapsforge.map.awt.graphics.AwtTileBitmap;
import org.mapsforge.map.layer.renderer.RendererJob;
import de.metager.mapsforge.MapsforgeManager;

import java.io.IOException;
import java.io.PipedOutputStream;

/**
 * @author SumaEV This class represents a Tile within our server In the
 *         constructor we provide the needed Information about the Tile that
 *         needs to get rendered. The method generateTile() starts the rendering
 *         process to a given Outputstream or serves the Tile from the Cache if
 *         possible.
 */
public class Tile implements Runnable {
	private org.mapsforge.core.model.Tile tile;
	private PipedOutputStream out;
	private MapsforgeManager mapsforgeManager;
	public Tile(org.mapsforge.core.model.Tile tile, MapsforgeManager mapsforgeManager, PipedOutputStream out) {
		this.tile = tile;
		this.mapsforgeManager = mapsforgeManager;
		this.out = out;
	}

	@Override
	public void run() {
		try {
			// The Tile cannot be loaded from the cache
			mapsforgeManager.getRenderThemeFuture().incrementRefCount();
	
			RendererJob rendererJob = new RendererJob(this.tile, mapsforgeManager.getMultiMapDataStore(), mapsforgeManager.getRenderThemeFuture(), mapsforgeManager.getDisplayModel(),
					(float) 1, false, false);
			AwtTileBitmap tileImage = (AwtTileBitmap) mapsforgeManager.getDatabaseRenderer().executeJob(rendererJob);
			if(tileImage != null)
				tileImage.compress(out);
			this.mapsforgeManager.getRenderThemeFuture().decrementRefCount();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

}
