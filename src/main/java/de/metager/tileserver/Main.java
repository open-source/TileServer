package de.metager.tileserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import de.metager.mapsforge.MapsforgeManager;
import de.metager.updater.UpdateReceiver;
import de.metager.updater.Updater;

/**
 * @author SumaEV 
 * This is a Java Tileserver. 
 * 
 * It can run in one of two modes:
 * 1. Without arguments (Tile Server Mode)
 * Without arguments this Program will answer to requests to port 63825. The Following commands are possible:
 * list-files;minLon;minLat;maxLon;maxLat - Will give a JSON Response which lists informations about the files to download for the given area
 * download-files;minLon;minLat;maxLon;maxLat - Will create and send a tar file containing above listed files (mapfiles and pregenerated png tiles)
 * download-assets - Will create and send a tar file containing our style.xml and assets for the maps.metager.de style
 * updater - Will create a process that takes updates from an instance running in updater mode 
 * 2. Updater Mode
 * To start this mode supply the jar file with the following three arguments
 * Tileserver Url (An Url that points to a running Tileserver instance i.e. localhost:63825)
 * Data Url (An Url that points to an *.osm.pbf File to use as datasource i.e. http://download.geofabrik.de/europe/germany/bremen-latest.osm.pbf)
 * The Updater will then start the following process
 *  1. Download *.osm.pbf File
 *  2. Sort *.osm.pbf File
 *  3. Split the *.osm.pbf File (Splits the File into a lot of smaller Files that can later be downloaded seperately by the app)
 *  4. Convert the splitted *.osm.pbf Files to *.map Files
 *  5. Send the *.map Files to the Tileserver instance
 *  6. Generate pregenerated Tiles for zoom levels 0-13 (Since it takes too long to generate them on the fly)
 *  7. Send the pregenerated Tiles to the Tileserver
 *  Sleep until the next update is due and start again with step 1. (Standard Update interval is 7 days)
 *  
 * 
 * The Server works without configuration for our needs. If we find the
 * time we'll provide the possibility to configure all of this
 * dynamically.
 */
public class Main {

	public static void main(String[] args) {
		
		

		// Distinct whether this is a Tileserver process or a Update process:
		if (args.length == 4) {
			// This will start a updater
			String tileserverUrl = args[0];
			String dataUrl = args[1];
			if(!dataUrl.endsWith(".osm.pbf")) {
				System.out.println("The Supplied URL to download OSM data seems to be incorrect: " + dataUrl);
				System.exit(-1);
			}
			String polyUrl = args[2];
			if(!polyUrl.endsWith(".poly")) {
				System.out.println("The Supplied URL to download Polygon data seems to be incorrect: " + dataUrl);
				System.exit(-1);
			}
			int maxMemoryGB = 0;
			try {
				maxMemoryGB = Integer.parseInt(args[3]);
			}catch(NumberFormatException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if(maxMemoryGB < 2) {
				System.err.println("You have not specified enough RAM that we are allowed to use. This program needs at least 2GB Ram to work correctly");
				System.exit(-1);
			}
			
			new Updater(tileserverUrl, dataUrl, polyUrl, maxMemoryGB);
		} else if(args.length == 0){
			// This lock will prevent that someone downloads Files while we are updating them or vise-versa
			ReadWriteLock updateLock = new ReentrantReadWriteLock();
			File pathPrefix = new File("tile-server-data");
			MapsforgeManager mapsforgeManager = new MapsforgeManager(updateLock, pathPrefix);
			Thread updaterThread = null;
			
			
			// Create the socket which will accept a new Connection
			// Each connection will be a tilerequest
			ServerSocket serverSocket = null;
			try {
				serverSocket = new ServerSocket(63825);
				while (true) {
					if (Thread.interrupted())
						break;
					Socket clientSocket = serverSocket.accept();
					clientSocket.setSoTimeout(100);
					String command = null;
					try {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(clientSocket.getInputStream(), Charset.forName("utf8")));
						command = reader.readLine();
					}catch(SocketTimeoutException e) {
						clientSocket.getOutputStream().write(new String("Bye!\n").getBytes());
						clientSocket.getOutputStream().flush();
						clientSocket.close();
						continue;
					}
					if(command.isEmpty() || command == null) {
						clientSocket.getOutputStream().write(new String("Bye!\n").getBytes());
						clientSocket.getOutputStream().flush();
						clientSocket.close();
						continue;
					}
					String arguments = "";
					if(command.contains(";")) {
						arguments = command.substring(command.indexOf(";") + 1);
						command = command.substring(0, command.indexOf(";"));
					}
					switch (command) {
					case "list-files":
						mapsforgeManager.listFilesToClient(clientSocket, arguments, updateLock);
						break;
					case "download-files":
						mapsforgeManager.downloadFilesToClient(clientSocket, arguments, updateLock);
						break;
					case "download-assets":
						mapsforgeManager.downloadAssetsToClient(clientSocket);
						break;
					case "updater":
						clientSocket.setSoTimeout(0);
						// Our Updater has connected It'll get its own process to send us new Data
						if(updaterThread == null || !updaterThread.isAlive()) {
							updaterThread = new Thread(new UpdateReceiver(clientSocket, mapsforgeManager));
							updaterThread.start();
						}else {
							// We already have an updater
							clientSocket.close();
						}
						break;
					default:
						mapsforgeManager.renderTileToClient(clientSocket, command + arguments);
						
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
