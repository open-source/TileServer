package de.metager.tileserver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.mapsforge.core.graphics.Canvas;
import org.mapsforge.core.mapelements.MapElementContainer;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import de.metager.mapsforge.MapsforgeManager;
import de.metager.updater.UpdateSender;

/**
 * @author SumaEV A simple HttpHandler. It initializes the Tile Cache and
 *         handles incoming HTTP-Requests If the requests are valid Tile
 *         requests it starts the rendering Job.
 */
public class TileWriter implements Runnable {
	private String tile;
	private File outputPath;
	private UpdateSender sender;
	private MapsforgeManager mapsforgeManager;
	private ExecutorService tileExecutor;
	private ExecutorService labelExecutor;

	public TileWriter(UpdateSender sender, String tile, File outputFile, MapsforgeManager mapsforgeManager, ExecutorService tileExecutor, ExecutorService labelExecutor) {
		this.sender = sender;
		this.tile = tile;
		this.outputPath = outputFile;
		this.mapsforgeManager = mapsforgeManager;
		this.tileExecutor = tileExecutor;
		this.labelExecutor = labelExecutor;
	}

	@Override
	public void run() {
		try {
			handleRequest();
		} catch (IOException | InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	private void handleRequest() throws IOException, InterruptedException, ExecutionException {
		outputPath.getParentFile().mkdirs();
		File tmpFile = null;
		// Read in the request
		try (OutputStream os = new FileOutputStream(outputPath)) {
			Pattern pattern = Pattern.compile("(\\d+);(\\d+);(\\d+)");
			Matcher m = pattern.matcher(tile);
			if (m.find()) {
				int z = Integer.parseInt(m.group(1));
				int x = Integer.parseInt(m.group(2));
				int y = Integer.parseInt(m.group(3));

				// There is no generated Tile yet. We need to generate it.
				BufferedImage image;
				List<MapElementContainer> labels = null;
				// Generate the Tile without labels
				org.mapsforge.core.model.Tile tile = new org.mapsforge.core.model.Tile(x, y, (byte) z, 256);
				try (PipedOutputStream out = new PipedOutputStream(); PipedInputStream in = new PipedInputStream(out)) {
					Tile newTile = new Tile(tile, mapsforgeManager, out);
					Future<?> tileFuture = tileExecutor.submit(newTile);
					// Get the Labels
					Future<List<MapElementContainer>> labelFetcher = labelExecutor.submit(new LabelFetcher(tile, mapsforgeManager));
			
					// Read the Labels
					labels = labelFetcher.get();
					// Read in the Tile without Label
					image = ImageIO.read(in);
					tileFuture.get();
					Canvas canvas = (Canvas) AwtGraphicFactory.createGraphicContext(image.getGraphics());
					for (MapElementContainer element : labels) {
						element.draw(canvas, tile.getOrigin(), mapsforgeManager.getGRAPHIC_FACTORY().createMatrix(),
								mapsforgeManager.getDisplayModel().getFilter());
					}
					
					ImageIO.write(image, "png", os);
				}

				
			}
		} finally {
			if (tmpFile != null) {
				FileUtils.forceDelete(tmpFile);
			}
			try {
				sender.submitNewTile(outputPath);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
