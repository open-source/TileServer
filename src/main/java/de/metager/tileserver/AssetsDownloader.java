package de.metager.tileserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class AssetsDownloader implements Runnable {

	private Socket clientSocket;
	private File renderThemeFile;

	public AssetsDownloader(Socket clientSocket, File renderThemeFile) {
		this.clientSocket = clientSocket;
		this.renderThemeFile = renderThemeFile;
	}

	@Override
	public void run() {
		long currentTime = System.currentTimeMillis();
		File outputFile = new File(System.getProperty("java.io.tmpdir") + "/" + currentTime + ".tar");
		try {
			outputFile.createNewFile();
			// Create an Array of all Files that need to be in the Archive (Assets Folder)
			@SuppressWarnings("unchecked")
			Collection<File> files = FileUtils.listFiles(new File(this.renderThemeFile.getParentFile(), "assets"),
					new WildcardFileFilter("*"), new WildcardFileFilter("*"));
			Iterator<File> iter = files.iterator();

			try (ArchiveOutputStream o = new TarArchiveOutputStream(new FileOutputStream(outputFile))) {

				// Pattern for the Filename
				Pattern p = Pattern.compile("^.*(/assets.*$)");

				while (iter.hasNext()) {
					File file = iter.next();
					Matcher m = p.matcher(file.getAbsolutePath());
					if (m.matches()) {
						ArchiveEntry entry = o.createArchiveEntry(file, m.group(1));
						o.putArchiveEntry(entry);
						if (file.isFile()) {
							try (InputStream i = Files.newInputStream(file.toPath())) {
								IOUtils.copy(i, o);
							}
						}
						o.closeArchiveEntry();
					}
				}
				// Put the XML Style file
				ArchiveEntry entry = o.createArchiveEntry(renderThemeFile, renderThemeFile.getName());
				o.putArchiveEntry(entry);
				try (InputStream i = Files.newInputStream(renderThemeFile.toPath())) {
					IOUtils.copy(i, o);
				}
				o.closeArchiveEntry();
				o.finish();
			}
			// Finally copy the tar to the Outputstream
			try (FileInputStream i = new FileInputStream(outputFile);
					OutputStream o = clientSocket.getOutputStream();) {
				IOUtils.copy(i, o);
			}
		} catch (IOException e) {
			Logger.Log(e);
		} finally {
			try {
				FileUtils.forceDelete(outputFile);
			} catch (IOException e1) {
				Logger.Log(e1);
			}
			try {
				this.clientSocket.close();
			} catch (IOException e) {
				Logger.Log(e);
			}
		}
	}

}
