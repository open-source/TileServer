package de.metager.tileserver;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.ReadWriteLock;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.Rectangle;
import org.mapsforge.map.datastore.MultiMapDataStore;
import org.mapsforge.map.datastore.MultiMapDataStore.DataPolicy;
import org.mapsforge.map.reader.MapFile;

import de.metager.mapsforge.MapsforgeHelper;

public class FileLister implements Runnable {

	final String arguments;
	final Socket clientSocket;
	
	private Rectangle rect;
	private File mapfilePath;
	private File tileCachePath;
	private ReadWriteLock updateLock;
	
	public FileLister(Socket clientSocket, String arguments, File mapFilePath, File tileCachePath, ReadWriteLock updateLock2) {
		this.arguments = arguments;
		this.clientSocket = clientSocket;
		this.mapfilePath = mapFilePath;
		this.tileCachePath = tileCachePath;
		this.updateLock = updateLock2;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		updateLock.readLock().lock();
		try {
			String[] locations = arguments.split(";");
			if(locations.length != 4) return;
			
			rect = new Rectangle(Double.parseDouble(locations[0]), Double.parseDouble(locations[1]), Double.parseDouble(locations[2]), Double.parseDouble(locations[3]));
			
			ArrayList<File> mapfiles = MapsforgeHelper.getMapFilesForBoundingBox(mapfilePath, rect);
			
			if(mapfiles.size() == 0)
				return;
			
			MultiMapDataStore datastore = new MultiMapDataStore(DataPolicy.DEDUPLICATE);
			
			for(File mapFile : mapfiles) {
				datastore.addMapDataStore(new MapFile(mapFile), false, false);
			}
			
			BoundingBox newbbox = datastore.boundingBox();
			datastore.close();
			mapfiles = MapsforgeHelper.getMapFilesForBoundingBox(mapfilePath, new Rectangle(newbbox.minLongitude, newbbox.minLatitude, newbbox.maxLongitude, newbbox.maxLatitude));
			
			
			long size = 0;
			
			
			for(File mapFile : mapfiles) {
				size += mapFile.length();
			}
			
			// For a correct Download Size we need to include the Tiles for zoom 0-13, too
			ArrayList<File> tiles = MapsforgeHelper.getTilesForBoundingBox(this.rect, this.tileCachePath);
			for(File tile : tiles) {
				size += tile.length();
			}
			
			File[] tilePaths = tileCachePath.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					return pathname.isDirectory();
				}
			});
			Arrays.sort(tilePaths);
			File tilePath = tilePaths[0];
			
			File[] mapFileDirs = mapfilePath.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					return pathname.isDirectory();
				}
			});
			Arrays.sort(mapFileDirs);
			File mapFilePath = mapFileDirs[0];
			
			long lastModified = Math.min(Long.parseLong(tilePath.getName()), Long.parseLong(mapFilePath.getName()));
			
			// Create the JSON output
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("size", size);
			jsonObject.put("last-modified", lastModified);
			
			JSONArray bbox = new JSONArray();
			bbox.add(rect.left);
			bbox.add(rect.top);
			bbox.add(rect.right);
			bbox.add(rect.bottom);
			
			jsonObject.put("original-bbox", bbox);
			
			JSONArray bboxNew = new JSONArray();
			bboxNew.add(newbbox.minLongitude);
			bboxNew.add(newbbox.minLatitude);
			bboxNew.add(newbbox.maxLongitude);
			bboxNew.add(newbbox.maxLatitude);
			
			jsonObject.put("bbox", bboxNew);
			
			JSONArray fileNames = new JSONArray();
			for(File mapFile : mapfiles) {
				fileNames.add(FilenameUtils.getBaseName(mapFile.getName()) );
			}
			
			jsonObject.put("files", fileNames);
			
			IOUtils.write(jsonObject.toJSONString(), clientSocket.getOutputStream());
			
		} catch (FileNotFoundException e) {
			Logger.Log(e);
		} catch (IOException e) {
			Logger.Log(e);
		} finally {
			updateLock.readLock().unlock();
			try {
				clientSocket.close();
			} catch (IOException e) {
				Logger.Log(e);
			}
		}
	}

}
