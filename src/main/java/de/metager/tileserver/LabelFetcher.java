package de.metager.tileserver;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import org.mapsforge.core.mapelements.MapElementContainer;
import org.mapsforge.core.model.Tile;
import org.mapsforge.map.util.LayerUtil;

import de.metager.mapsforge.MapsforgeManager;

public class LabelFetcher implements Callable<List<MapElementContainer>> {

	private MapsforgeManager mapsforgeManager;
	private Tile tile;

	public LabelFetcher(Tile tile, MapsforgeManager mapsforgeManager) {
		this.tile = tile;
		this.mapsforgeManager = mapsforgeManager;
	}

	@Override
	public List<MapElementContainer> call() throws Exception {
		Tile tileTop = tile.getAboveLeft();
		Tile tileBottom = tile.getBelowRight();
		
		List<MapElementContainer> elements = null;
		for(int xTile = tileTop.tileX; xTile <= tileBottom.tileX; xTile++) {
			for(int yTile = tileTop.tileY; yTile <= tileBottom.tileY; yTile++) {
				if(elements == null)
					elements = mapsforgeManager.getLabelStore().getVisibleItems(new Tile(xTile,yTile,tile.zoomLevel,256), new Tile(xTile,yTile,tile.zoomLevel,256));
				else
					elements.addAll(mapsforgeManager.getLabelStore().getVisibleItems(new Tile(xTile,yTile,tile.zoomLevel,256), new Tile(xTile,yTile,tile.zoomLevel,256)));
			}
		}
		elements = LayerUtil.collisionFreeOrdered(elements);
		Collections.sort(elements);
		return elements;
	}

}
