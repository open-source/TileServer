package de.metager.tileserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.Rectangle;
import org.mapsforge.map.datastore.MultiMapDataStore;
import org.mapsforge.map.datastore.MultiMapDataStore.DataPolicy;
import org.mapsforge.map.reader.MapFile;

import de.metager.mapsforge.MapsforgeHelper;

public class FileDownloader implements Runnable {

	private Socket clientSocket;
	private String arguments;
	private File mapFilePath;
	private File tileCachePath;
	private ReadWriteLock updateLock;

	public FileDownloader(Socket clientSocket, String arguments, File mapFilePath, File tileCachePath, ReadWriteLock updateLock) {
		this.clientSocket = clientSocket;
		this.arguments = arguments;
		this.mapFilePath = mapFilePath;
		this.tileCachePath = tileCachePath;
		this.updateLock = updateLock;
	}

	@Override
	public void run() {
		long currentTime = System.currentTimeMillis();
		File outputFile = new File(System.getProperty("java.io.tmpdir") + "/" + currentTime + ".tar");
		updateLock.readLock().lock();
		try {

			outputFile.createNewFile();

			String[] locations = arguments.split(";");
			if (locations.length != 4)
				return;

			Rectangle rect = new Rectangle(Double.parseDouble(locations[0]), Double.parseDouble(locations[1]),
					Double.parseDouble(locations[2]), Double.parseDouble(locations[3]));
			ArrayList<File> mapfiles = MapsforgeHelper.getMapFilesForBoundingBox(mapFilePath, rect);
			
			if(mapfiles.size() == 0)
				return;
			
			MultiMapDataStore datastore = new MultiMapDataStore(DataPolicy.DEDUPLICATE);
			
			for(File mapFile : mapfiles) {
				datastore.addMapDataStore(new MapFile(mapFile), false, false);
			}
			BoundingBox bbox = datastore.boundingBox();
			datastore.close();
			rect = new Rectangle(bbox.minLongitude, bbox.minLatitude, bbox.maxLongitude, bbox.maxLatitude);
			mapfiles = MapsforgeHelper.getMapFilesForBoundingBox(mapFilePath, rect);

			try (ArchiveOutputStream o = new TarArchiveOutputStream(new FileOutputStream(outputFile))) {
				String mapFilePrefix = "/" + currentTime + "/mapfiles/";
				for (File file : mapfiles) {
					ArchiveEntry entry = o.createArchiveEntry(file, mapFilePrefix + file.getName());
					o.putArchiveEntry(entry);
					if (file.isFile()) {
						try (InputStream i = Files.newInputStream(file.toPath())) {
							IOUtils.copy(i, o);
						}
					}
					o.closeArchiveEntry();
				}

				String tilePrefix = "/" + currentTime + "/tiles";
				ArrayList<File> tiles = MapsforgeHelper.getTilesForBoundingBox(rect, this.tileCachePath);

				// Regex for extracting the correct Path
				Pattern p = Pattern.compile("^.*((/[^/]+){3})$");

				for (File tile : tiles) {
					Matcher m = p.matcher(tile.getAbsolutePath());
					if (m.matches()) {
						ArchiveEntry entry = o.createArchiveEntry(tile, tilePrefix + m.group(1));
						o.putArchiveEntry(entry);
						if (tile.isFile()) {
							try (InputStream i = Files.newInputStream(tile.toPath())) {
								IOUtils.copy(i, o);
							}
						}
						o.closeArchiveEntry();
					}

				}
				o.finish();
			}

			// Finally copy the tar to the Outputstream
			try (FileInputStream i = new FileInputStream(outputFile);
					OutputStream o = clientSocket.getOutputStream();) {
				IOUtils.copy(i, o);
			}

		} catch (FileNotFoundException e) {
			Logger.Log(e);
		} catch (IOException e) {
			Logger.Log(e);
		} finally {
			updateLock.readLock().unlock();
			try {
				FileUtils.forceDelete(outputFile);
			} catch (IOException e1) {
				Logger.Log(e1);
			}
			try {
				clientSocket.close();
			} catch (IOException e) {
				Logger.Log(e);
			}
		}
	}

}
