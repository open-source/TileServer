package de.metager.tileserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {
	public synchronized static void Log(Exception e) {
		PrintWriter writer;
		try {
			File logFile = new File("tileserver-log");
			if(!logFile.exists()) {
				logFile.createNewFile();
			}
			writer = new PrintWriter(new FileOutputStream(new File("tileserver-log"), true));
			e.printStackTrace(writer);
			writer.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
}
