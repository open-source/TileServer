package de.metager.tileserver;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.mapsforge.core.graphics.Canvas;
import org.mapsforge.core.mapelements.MapElementContainer;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;

import de.metager.mapsforge.MapsforgeManager;

/**
 * @author SumaEV A simple HttpHandler. It initializes the Tile Cache and
 *         handles incoming HTTP-Requests If the requests are valid Tile
 *         requests it starts the rendering Job.
 */
public class TileRenderer implements Runnable {

	private Socket clientSocket;
	private String command;
	private MapsforgeManager mapsforgeManager;
	private ExecutorService tileExecutor;
	private int x,y,z;

	public TileRenderer(MapsforgeManager mapsforgeManager, Socket clientSocket, String command,
			ExecutorService tileExecutor) {
		this.mapsforgeManager = mapsforgeManager;
		this.clientSocket = clientSocket;
		this.command = command;
		this.tileExecutor = tileExecutor;
	}

	@Override
	public void run() {
		mapsforgeManager.getMapFileLock().readLock().lock();
		Boolean needsUpdate = null;
		try {
			needsUpdate = handleRequest();
		} catch (IOException | InterruptedException | ExecutionException e) {
			Logger.Log(e);
		} finally {
			mapsforgeManager.getMapFileLock().readLock().unlock();
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(needsUpdate) {
			mapsforgeManager.getMapFileLock().readLock().lock();
			// Update the Lastmodified so no other process will render this
			try(Statement stmt = mapsforgeManager.getCon().createStatement()){
				stmt.executeUpdate("UPDATE tiles SET `lastModified` = " + mapsforgeManager.getDataAge() + " WHERE `x`= " + x + " AND" + "`y` = " + y + " AND" + "`z` = " + z);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				renderTile();
			} catch (IOException | InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}finally {
				mapsforgeManager.getMapFileLock().readLock().unlock();
			}
		}
	}

	private boolean handleRequest() throws IOException, InterruptedException, ExecutionException {
		// Read in the request
		try (OutputStream os = clientSocket.getOutputStream()) {
			Pattern pattern = Pattern.compile("(\\d+);(\\d+);(\\d+)");
			Matcher m = pattern.matcher(command);
			if (m.find()) {
				z = Integer.parseInt(m.group(1));
				x = Integer.parseInt(m.group(2));
				y = Integer.parseInt(m.group(3));

				// Check if Tile is already Rendered
				try (Statement stmt = mapsforgeManager.getCon().createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM `tiles` WHERE `x`= " + x + " AND" + "`y` = " + y + " AND" + "`z` = " + z)) {
					if (rs.next()) {
						byte[] image = rs.getBytes("image") ;
						try (InputStream in = new ByteArrayInputStream(image);) {
							IOUtils.copy(in, os);
							image = null;
							long databaseAge = rs.getLong("lastModified");
							long dataAge = mapsforgeManager.getDataAge();
							
							if(databaseAge < dataAge)
								return true;
							else 
								return false;
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

				BufferedImage image = renderTile();
				ImageIO.write(image, "png", os);
				
			}
		}
		return false;

	}
	
	private BufferedImage renderTile() throws IOException, InterruptedException, ExecutionException {
		// There is no generated Tile yet. We need to generate it.
		BufferedImage image;
		try (PipedOutputStream out = new PipedOutputStream();
				PipedInputStream in = new PipedInputStream(out);) {
			// Generate the Tile without labels
			org.mapsforge.core.model.Tile tile = new org.mapsforge.core.model.Tile(x, y, (byte) z, 256);
			Tile newTile = new Tile(tile, mapsforgeManager, out);
			Future<?> tileFuture = tileExecutor.submit(newTile);
			// Get the Labels
			Future<List<MapElementContainer>> labelFetcher = tileExecutor
					.submit(new LabelFetcher(tile, mapsforgeManager));

			// Read the Labels
			List<MapElementContainer> labels = labelFetcher.get();
			// Read in the Tile without Label
			image = ImageIO.read(in);
			tileFuture.get();
			Canvas canvas = (Canvas) AwtGraphicFactory.createGraphicContext(image.getGraphics());
			for (MapElementContainer element : labels) {
				element.draw(canvas, tile.getOrigin(), mapsforgeManager.getGRAPHIC_FACTORY().createMatrix(),
						mapsforgeManager.getDisplayModel().getFilter());
			}
		}
		if (z <= 13) {
			/*
			 * Save the File for Later Use tileFile.getParentFile().mkdirs();
			 */
			try(PreparedStatement stmt = mapsforgeManager.getCon().prepareStatement("INSERT OR REPLACE INTO tiles values(" + x + "," + y + "," + z + ",?, " + mapsforgeManager.getDataAge() + ")");){
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ImageIO.write(image, "png", bos);
				stmt.setBytes(1, bos.toByteArray());
				stmt.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return image;
		
	}
}
