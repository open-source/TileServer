package de.metager.updater;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import de.metager.mapsforge.MapsforgeManager;

public class UpdateReceiver implements Runnable {

	private Socket clientSocket;
	private MapsforgeManager mapsforgeManager;
	private BufferedInputStream reader;
	private BufferedWriter writer;

	public UpdateReceiver(Socket clientSocket, MapsforgeManager mapsforgeManager) {
		this.clientSocket = clientSocket;
		this.mapsforgeManager = mapsforgeManager;
	}

	@Override
	public void run() {
		File outputFile = null;
		try {
			this.reader = new BufferedInputStream(clientSocket.getInputStream());
			// Confirm connection
			this.writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			writer.write("Ready\n");
			writer.flush();

			while (true) {
				String line = this.readLine();
				if (line == null)
					break;
				line = line.trim();
				// The Line determines what will come next
				if (line.equals("mapfiles-start"))
					this.receiveMapFiles();
				else if (line.equals("tiles-start"))
					this.reveiveTiles();

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (outputFile != null)
					FileUtils.forceDelete(outputFile);
				writer.flush();
				writer.close();
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void reveiveTiles() throws IOException {
		String newName = Long.toString(System.currentTimeMillis());
		File outputDir = new File(mapsforgeManager.getPrerenderedTilePath(), newName);
		outputDir.mkdirs();
		FileUtils.cleanDirectory(outputDir);

		Pattern p = Pattern.compile("^(\\d+)-(\\d+)-(\\d+)$");
		while (true) {
		
			String line = this.readLine();
			if (line == null) {
				break;
			}
			line = line.trim();
			if (line.equals("tiles-end")) {
				// Delete the previous png File
				File[] tilePaths = mapsforgeManager.getPrerenderedTilePath().listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.isDirectory() && !pathname.getName().equals(newName);
					}
				});
				for (File tilePath : tilePaths) {
					FileUtils.deleteDirectory(tilePath);
				}
				// Send Confirmation that we updated successfully
				writer.write("Saved.\n");
				writer.flush();
				break;
			}
			String[] data = null;
			if(line.indexOf(";") > -1) {
				data = line.trim().split(";");
			}
			if(data == null || data.length != 3)
				throw new IOException("Wrong Parameter count");
			int len = Integer.parseInt(data[2]);
			byte[] buffer = new byte[4096];

			Matcher m = p.matcher(data[1]);
			if (m.matches()) {
				int z = Integer.parseInt(m.group(1));
				int x = Integer.parseInt(m.group(2));
				int y = Integer.parseInt(m.group(3));

				File outputFile = new File(outputDir, z + "/" + x + "/" + y + ".png");
				outputFile.getParentFile().mkdirs();
				outputFile.createNewFile();
				try (OutputStream out = new FileOutputStream(outputFile)) {
					while (len > 0) {
						int readTmp = reader.read(buffer, 0, Math.min(4096, len));
						if (readTmp == -1)
							throw new IOException("Connection closed");
						out.write(buffer, 0, readTmp);
						out.flush();
						len -= readTmp;
					}
					// Delete the previous png File
					File[] tilePaths = mapsforgeManager.getPrerenderedTilePath().listFiles(new FileFilter() {
						@Override
						public boolean accept(File pathname) {
							return pathname.isDirectory() && !pathname.getName().equals(newName);
						}
					});
					for (File tilePath : tilePaths) {
						File tileFile = new File(tilePath, z + "/" + x + "/" + y + ".png");
						if (tileFile.exists())
							FileUtils.forceDelete(tileFile);
					}
				}
			}
		}
	}

	private String readLine() throws IOException {
		ArrayList<Byte> result = new ArrayList<>();
		String res = "";
		byte[] buffer = new byte[1];
		while (true) {
			int tmp = reader.read(buffer, 0, 1);
			if (tmp == 0)
				continue;
			else if (tmp == -1)
				throw new IOException("Connection closed");
			result.add(buffer[0]);

			byte[] stockArr = new byte[result.size()];
			for (int i = 0; i < result.size(); i++) {
				stockArr[i] = result.get(i);
			}
			res = new String(stockArr);
			if (res.endsWith("\n"))
				break;
		}
		return res.trim();
	}

	private void receiveMapFiles() throws IOException {
		String newName = Long.toString(System.currentTimeMillis());
		File outputDir = new File(mapsforgeManager.getMapFilePath(), newName);
		outputDir.mkdirs();
		try {
			while (true) {
				String line = this.readLine();
				if (line == null) {
					break;
				}
				line = line.trim();
				if (line.equals("mapfiles-end")) {
					break;
				}

				String[] data = line.trim().split(";");
				int len = Integer.parseInt(data[2]);
				byte[] buffer = new byte[4096];

				File outputFile = new File(outputDir, data[1]);
				outputFile.createNewFile();
				try (OutputStream out = new FileOutputStream(outputFile)) {
					while (len > 0) {
						int readTmp = reader.read(buffer, 0, Math.min(4096, len));
						if (readTmp == -1)
							throw new IOException("Connection closed");
						out.write(buffer, 0, readTmp);
						out.flush();
						len -= readTmp;
					}
				}
			}
			mapsforgeManager.getUpdateLock().writeLock().lock();
			mapsforgeManager.getMapFileLock().writeLock().lock();
			try {
				
				File[] mapFileFolders = mapsforgeManager.getMapFilePath().listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.isDirectory() && !pathname.getName().equals(newName);
					}
				});
				for(File mapFileFolder : mapFileFolders) {
					FileUtils.deleteDirectory(mapFileFolder);
				}
				mapsforgeManager.updateMapFiles(outputDir);
			}finally {
				mapsforgeManager.getUpdateLock().writeLock().unlock();
				mapsforgeManager.getMapFileLock().writeLock().unlock();
			}
			writer.write("Saved.\n");
			writer.flush();
		} catch (IOException e) {
			FileUtils.deleteDirectory(outputDir);
			throw e;
		}

	}

}
