package de.metager.updater;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class UpdateSender {

	private Socket tileServerSocket = null;
	private ArrayBlockingQueue<File> sendingQueue = new ArrayBlockingQueue<>(1000);
	private ReentrantLock writeLock = new ReentrantLock();
	private BufferedOutputStream out;
	private BufferedReader in;
	private String host;
	private int port;

	public UpdateSender(String host, int port) {
		this.host = host;
		this.port = port;
		this.connect();
	}

	private void connect() {
		boolean connected = false;
		if (this.tileServerSocket != null) {
			try {
				out.flush();
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			out = null;
			in = null;
			try {

				tileServerSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			tileServerSocket = null;
		}
		while (!connected) {
			try {
				this.tileServerSocket = new Socket(host, port);
				this.out = new BufferedOutputStream(tileServerSocket.getOutputStream());
				this.in = new BufferedReader(new InputStreamReader(tileServerSocket.getInputStream()));
				out.write(new String("updater\n").getBytes());
				out.flush();
				// We need to wait until the server aknowledged us and is ready to be used
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(tileServerSocket.getInputStream(), Charset.forName("UTF-8")));
				reader.read();
				break;
			} catch (IOException e) {
				this.out = null;
				this.tileServerSocket = null;
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					System.exit(-1);
				}
			}
		}

	}

	public void submitTilesDynamic(ThreadPoolExecutor executor, File prerenderedPath) {
		// Message the Tileserver that new Data is incoming
		File file = null;
		Pattern tilePattern = Pattern.compile("^.*/(\\d+)/(\\d+)/(\\d+)\\.png$");
		try {
			writeLock.lock();
			out.write(new String("tiles-start\n").getBytes());
			out.flush();

			int queuedProcesses = 0;
			while (!executor.isTerminated() || !sendingQueue.isEmpty()) {
				int queuedTmp = 0;
				if(executor.getQueue().size() > 0) {
					queuedTmp = executor.getQueue().size();
				}else if(!sendingQueue.isEmpty()) {
					queuedTmp = sendingQueue.size();
				}
				if(queuedTmp != queuedProcesses) {
					queuedProcesses = queuedTmp;
					System.out.println(
							"[" + Instant.now() + "] " + Integer.toString(queuedProcesses) + " Tiles remaining.");
				}
				
				file = sendingQueue.poll(1, TimeUnit.SECONDS);
				if (file == null)
					continue;
				byte[] buffer = new byte[4096];
				long size = file.length();
				Matcher m = tilePattern.matcher(file.getAbsolutePath());
				if(m.matches()) {
					out.write(new String("tile;" + m.group(1) + "-" + m.group(2) + "-" + m.group(3) + ";" + size + "\n").getBytes());
					out.flush();
					try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
						int read = 0;
						while ((read = in.read(buffer, 0, 4096)) != -1) {
							out.write(buffer, 0, read);
							size -= read;
						}
						out.flush();
					}
				}else {
					throw new IOException("Couldn't parse Pathname of Tile");
				}
				buffer = null;
			}

			out.write(new String("tiles-end\n").getBytes());
			out.flush();
			// Wait for confirmation that everything got saved Properly
			String result = in.readLine();
			if (!result.equals("Saved.")) {
				writeLock.unlock();
				submitTiles(prerenderedPath);
			}
		} catch (IOException e) {
			// There was a Problem writing the File to the server
			int queuedProcesses = 0;
			while(!executor.isTerminated()) {
				try {
					int queuedTmp = 0;
					if(executor.getQueue().size() > 0) {
						queuedTmp = executor.getQueue().size();
					}else if(!sendingQueue.isEmpty()) {
						queuedTmp = sendingQueue.size();
					}
					if(queuedTmp != queuedProcesses) {
						queuedProcesses = queuedTmp;
						System.out.println(
								"[" + Instant.now() + "] " + Integer.toString(queuedProcesses) + " Tiles remaining.");
					}
					sendingQueue.clear();
					executor.awaitTermination(1, TimeUnit.SECONDS);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			sendingQueue.clear();
			writeLock.unlock();
			this.connect();
			submitTiles(prerenderedPath);
		} catch (InterruptedException e) {
			writeLock.unlock();
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private void submitTiles(File prerenderedPath) {
		// Retry to send the Tiles
		File[] zoomFiles = prerenderedPath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});
		while(true) {
			try {
				writeLock.lock();
				out.write(new String("tiles-start\n").getBytes());
				out.flush();
				String[] extensions = {"png"};
				int filesToSend = FileUtils.listFiles(prerenderedPath, extensions, true).size();
				
				for(File zoom : zoomFiles) {
					File[] xFiles = zoom.listFiles(new FileFilter() {
						@Override
						public boolean accept(File pathname) {
							return pathname.isDirectory();
						}
					});
					for(File x : xFiles) {
						File[] yFiles = x.listFiles(new FileFilter() {
							@Override
							public boolean accept(File pathname) {
								return pathname.isFile() && pathname.getName().endsWith(".png");
							}
						});
						for(File tileFile : yFiles) {
							byte[] buffer = new byte[4096];
							long size = tileFile.length();
							out.write(new String("tile;" + zoom.getName() + "-" + x.getName() + "-" + FilenameUtils.getBaseName(tileFile.getName()) + ";" + size + "\n").getBytes());
							out.flush();
							try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(tileFile))) {
								int read = 0;
								while ((read = in.read(buffer, 0, 4096)) != -1) {
									out.write(buffer, 0, read);
									size -= read;
								}
								out.flush();
							}
							System.out.println(
									"[" + Instant.now() + "] " + Integer.toString(filesToSend) + " Tiles due to send.");
							filesToSend--;
						}
					}
				}
				out.write(new String("tiles-end\n").getBytes());
				out.flush();
				// Wait for confirmation that everything got saved Properly
				String result = in.readLine();
				if (result.equals("Saved.")) {
					break;
				}
			} catch (IOException e) {
				// There was a Problem writing the File to the server
				this.connect();
			}finally {
				writeLock.unlock();
			}
		}
	}

	public synchronized void submitNewTile(File mapFile) throws InterruptedException {
		this.sendingQueue.put(mapFile);
	}

	public void submitNewMapfiles(File[] mapFiles) {
		while (true) {

			try {
				writeLock.lock();
				out.write(new String("mapfiles-start\n").getBytes());
				out.flush();
				for (File mapFile : mapFiles) {
					byte[] buffer = new byte[4096];
					long size = mapFile.length();
					out.write(new String("mapfile;" + mapFile.getName() + ";" + size + "\n").getBytes());
					out.flush();
					try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(mapFile))) {
						int read = 0;
						while ((read = in.read(buffer, 0, 4096)) != -1) {
							out.write(buffer, 0, read);
							size -= read;
						}
						out.flush();
					}
				}
				out.write(new String("mapfiles-end\n").getBytes());
				out.flush();
				// Wait for confirmation that everything got saved Properly
				String result = in.readLine();
				if (result.equals("Saved.")) {
					break;
				}
			} catch (IOException e) {
				// There was a Problem writing the File to the server
				this.connect();
			} finally {
				writeLock.unlock();
			}
		}
	}

	public void exit() {
		while (!this.sendingQueue.isEmpty() || this.writeLock.isLocked()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			this.tileServerSocket.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
