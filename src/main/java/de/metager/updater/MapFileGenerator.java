package de.metager.updater;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.mapsforge.core.model.BoundingBox;

public class MapFileGenerator implements Runnable {

	private File osmFile;
	private File mapFile;
	private BoundingBox bbox;
	private File osmosisExecutable;
	public MapFileGenerator(File osmosisExecutable, File osmFile, File mapFile, BoundingBox bbox) {
		this.osmosisExecutable = osmosisExecutable;
		this.osmFile = osmFile;
		this.mapFile = mapFile;
		this.bbox = bbox;
	}

	@Override
	public void run() {	
		
		while(true) {
			ProcessBuilder pb = new ProcessBuilder();
			// Linux Dependency
			// TODO make it work for Windows
			pb.redirectOutput(new File("/dev/null"));
			
			// Read in the OSM File, the water file and the shape osm files write out the mapfile for them
			ArrayList<String> command = new ArrayList<>();
			
			command.add(osmosisExecutable.getAbsolutePath());
			command.add("--rbf"); 
			command.add("file=" + osmFile.getAbsolutePath());
			
			// Now the Mapfile writing
			command.add("--mapfile-writer");
			command.add("file=" + mapFile.getAbsolutePath());
			command.add("preferred-languages=de");
			command.add("type=ram");
			command.add("bbox=" + bbox.minLatitude + "," + bbox.minLongitude + "," + bbox.maxLatitude + "," + bbox.maxLongitude);
			
			
			pb.command(command);
			
			try {
				if(mapFile.exists())
					FileUtils.forceDelete(mapFile);
				Process p = pb.start();
				p.waitFor();
				p.destroy();
				if(p.exitValue() == 0) { // Successfully built the mapfile
					FileUtils.forceDelete(this.osmFile);
					break;
				}else { // An Error occured
					FileUtils.forceDelete(mapFile);
				}
				
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
