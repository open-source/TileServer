package de.metager.updater;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.mapsforge.core.model.BoundingBox;
import de.metager.mapsforge.MapsforgeHelper;
import de.metager.mapsforge.MapsforgeManager;
import de.metager.tileserver.Main;
import de.metager.tileserver.TileWriter;

public class Updater {

	private final File dataDir = new File("updater-data");
	private final File splittedPath = new File(dataDir, "splitted");
	private final File pregeneratedPath = new File(dataDir, "pregenerated");
	private final File osmosisPath = new File(dataDir, "osmosis/bin/osmosis");
	private final File osmosisTemp = new File(dataDir, "temp");
	private final File splitterPath = new File(dataDir, "splitter-r591/splitter.jar");
	private final File landShapeFile = new File(dataDir, "shapefiles/land_polygons.shp");
	private final File shape2osmPathLand = new File(dataDir, "shape2osm-land.py");
	private final File osmosisOptions = new File(System.getProperty("user.home") + "/.osmosis");
	private final File lockFile = new File(dataDir, ".updated");
	private final int maxThreads = Runtime.getRuntime().availableProcessors();
	private File osmFile = new File(dataDir, "osm-data.osm.pbf");
	private final File polyFile = new File(dataDir, "osm-data.poly");
	private URL dataUrl;
	private UpdateSender sender;
	private MapsforgeManager mapsforgeManager;
	private int maxMemoryGB;
	private URL polyUrl;
	private BoundingBox bbox;

	public Updater(String tileserverUrl, String dataUrl, String polyUrl, int maxMemoryGB) {

		String[] tileserver = tileserverUrl.split(":");
		try {
			this.checkOgr();
			this.checkPython();
			this.dataUrl = new URL(dataUrl);
			this.polyUrl = new URL(polyUrl);
			this.maxMemoryGB = maxMemoryGB;
			this.sender = new UpdateSender(tileserver[0], Integer.parseInt(tileserver[1]));
			// Create the temporary data dir
			dataDir.mkdirs();
			osmosisTemp.mkdirs();
			splittedPath.mkdirs();
			this.checkOsmosis();
			this.checkSplitter();
			this.checkShapefiles();
			this.checkOgr();
			this.checkPython();
			this.startUpdater();

		} catch (NumberFormatException | IOException | InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private void checkShapefiles() throws FileNotFoundException, IOException {
		if (!shape2osmPathLand.exists()) {
			shape2osmPathLand.createNewFile();
			try (FileOutputStream out = new FileOutputStream(shape2osmPathLand);
					InputStream in = Main.class.getResourceAsStream("/shape2osm-land.py")) {
				IOUtils.copy(in, out);
			}
		}
		if (!landShapeFile.exists()) {
			landShapeFile.getParentFile().mkdirs();
			// Download the Shape Files
			File shapeZip = new File(landShapeFile.getParentFile(), "shapefiles.zip");
			shapeZip.createNewFile();
			URL downloadUrl = new URL("http://data.openstreetmapdata.com/land-polygons-split-4326.zip");
			try (InputStream in = downloadUrl.openStream(); FileOutputStream out = new FileOutputStream(shapeZip)) {
				IOUtils.copy(in, out);
			}
			// Unzip the Shape File
			try (ZipInputStream zin = new ZipInputStream(new FileInputStream(shapeZip))) {
				ZipEntry entry;
				while ((entry = zin.getNextEntry()) != null) {
					File newFile = new File(this.landShapeFile.getParentFile(),
							entry.getName().substring(entry.getName().lastIndexOf("/") + 1));
					if (newFile.isDirectory()) {
						newFile.mkdirs();
					} else {
						newFile.getParentFile().mkdirs();
						newFile.createNewFile();
						try (FileOutputStream out = new FileOutputStream(newFile)) {
							IOUtils.copy(zin, out);
						}
					}
				}
			} finally {
				FileUtils.forceDelete(shapeZip);
			}
		}
	}

	private void checkSplitter() throws IOException {
		if (!splitterPath.exists()) {
			try (ZipInputStream zis = new ZipInputStream(Main.class.getResourceAsStream("/splitter-r591.zip"));) {
				ZipEntry entry;
				while ((entry = zis.getNextEntry()) != null) {
					File newFile = new File(dataDir, entry.getName());
					if (!entry.isDirectory()) {
						newFile.getParentFile().mkdirs();
						try (FileOutputStream out = new FileOutputStream(newFile)) {
							IOUtils.copy(zis, out);
						}
					} else {
						newFile.mkdirs();
					}
				}
			}
		}
	}

	private void checkPython() throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("python", "-h");
		Process p = pb.start();
		p.waitFor();
		if (p.exitValue() != 0) {
			System.err.println("Seems like you haven't installed Python. Please install Python to continue!");
			System.exit(-1);
		}
	}

	private void checkOgr() throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("ogr2ogr", "--help-general");
		Process p = pb.start();
		p.waitFor();
		if (p.exitValue() != 0) {
			System.err.println("Seems like you haven't installed gdal. Please install gdal to continue!");
			System.exit(-1);
		}
	}

	private void checkOsmosis() throws IOException {
		if (this.osmosisPath.exists())
			return;

		try (ZipInputStream zis = new ZipInputStream(Main.class.getResourceAsStream("/osmosis.zip"));) {
			ZipEntry entry;
			while ((entry = zis.getNextEntry()) != null) {
				File newFile = new File(dataDir, entry.getName());
				if (!entry.isDirectory()) {
					newFile.getParentFile().mkdirs();
					try (FileOutputStream out = new FileOutputStream(newFile)) {
						IOUtils.copy(zis, out);
					}
				} else {
					newFile.mkdirs();
				}
			}
		}
		// Make Osmosis Files Executable
		this.osmosisPath.setExecutable(true);
	}

	@SuppressWarnings("unused")
	private void startUpdater() throws IOException, InterruptedException {
		try {
			while (true) {
				// Check if we need to update our data
				long maxAge = TimeUnit.DAYS.toMillis(7);
				if (!lockFile.exists() || (System.currentTimeMillis() - lockFile.lastModified()) >= maxAge) {
					update();
				} else {
					long sleepTime = maxAge - (System.currentTimeMillis() - lockFile.lastModified());
					System.out.println("Sleeping for " + sleepTime + " milliseconds.");
					while (true) {
						sleepTime = maxAge - (System.currentTimeMillis() - lockFile.lastModified());
						if (sleepTime <= 0) {
							break;
						} else {
							sleepTime = Math.min(10000, sleepTime);
							Thread.sleep(sleepTime);
						}
					}
				}
			}
		} finally {
			sender.exit();
		}
	}

	private void update() throws IOException, InterruptedException {
		// Download the new File
		System.out.println("[" + Instant.now() + "] " + "Downloading OSM File!");
		// downloadOsmFile();
		System.out.println("[" + Instant.now() + "] " + "Downloading OSM File!");
		mergeShapes();
		// Split the pbf file
		System.out.println("[" + Instant.now() + "] " + "Splitting OSM File!");
		splitOsmFile();
		// Generate the mapfiles
		System.out.println("[" + Instant.now() + "] " + "Generating Map Files!");
		generateMapFiles();
		// Generate the tiles
		System.out.println("[" + Instant.now() + "] " + "Generating Tiles!");
		generateTiles();

		FileUtils.cleanDirectory(this.splittedPath);

		FileUtils.touch(lockFile);
	}

	private void mergeShapes() throws IOException, InterruptedException {

		FileUtils.cleanDirectory(this.splittedPath);
		this.bbox = this.calculateBBox();
		File newShape = new File(this.splittedPath, "shape.shp");

		// Export Memory and Tempdir Options for Osmosis
		FileUtils.writeStringToFile(osmosisOptions, "export JAVACMD_OPTIONS=\"-Xmx" + this.maxMemoryGB
				+ "G -Djava.io.tmpdir=" + this.osmosisTemp.getAbsolutePath() + "\"");

		this.splitShapes(this.landShapeFile, newShape, bbox);

		ExecutorService executor = Executors.newSingleThreadExecutor();

		executor.submit(new Shape2Osm(newShape, bbox, this));

		executor.shutdown();
		while (!executor.isTerminated())
			executor.awaitTermination(1000, TimeUnit.SECONDS);

		// Now merge the OSM File with the Shapes
		ProcessBuilder pb = new ProcessBuilder();
		pb.redirectOutput(new File("/dev/null"));
		ArrayList<String> command = new ArrayList<>();
		command.add(this.osmosisPath.getAbsolutePath());
		command.add("--rbf");
		command.add("file=" + this.osmFile.getAbsolutePath());

		File[] shapeFiles = this.splittedPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("shape.");
			}
		});
		for (File file : shapeFiles) {
			command.add("--rx");
			command.add("file=" + file.getAbsolutePath());
			command.add("enableDateParsing=no");
			command.add("--s");
			command.add("--m");
		}
		File newOsmFile = new File(this.splittedPath, "osm-data.osm.pbf");
		command.add("--wb");
		command.add("file=" + newOsmFile.getAbsolutePath());

		pb.command(command);
		Process p = pb.start();

		p.waitFor();

		if (p.exitValue() != 0)
			throw new IOException();

		for (File file : shapeFiles) {
			FileUtils.forceDelete(file);
		}

	}

	private void generateTiles() throws IOException {
		// Find out which Bounding Box should get prerendered
		BoundingBox bbox = MapsforgeHelper.generateBoundingBox(splittedPath);
		this.mapsforgeManager = new MapsforgeManager(dataDir, this.pregeneratedPath, new File(dataDir, "tile-cache"),
				this.splittedPath, new File(dataDir, "render-theme"));
		System.out.println("[" + Instant.now() + "] Starting prerendering with " + maxThreads + " processes");
		LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
		ThreadPoolExecutor executor = new ThreadPoolExecutor(maxThreads, maxThreads * 2, 5, TimeUnit.SECONDS, queue);
		ExecutorService tileExecutor = Executors.newCachedThreadPool();
		ExecutorService labelExecutor = Executors.newCachedThreadPool();

		int startZoom = 0;
		int endZoom = 13;

		if (!pregeneratedPath.exists()) {
			pregeneratedPath.mkdirs();
		} else {
			FileUtils.cleanDirectory(pregeneratedPath);
		}

		for (int currentZoom = startZoom; currentZoom <= endZoom; currentZoom++) {
			int minX = (int) Math.floor((bbox.minLongitude + 180) / 360 * (1 << currentZoom));
			int maxX = (int) Math.floor((bbox.maxLongitude + 180) / 360 * (1 << currentZoom));
			if (minX < 0)
				minX = 0;
			if (minX >= (1 << currentZoom))
				minX = ((1 << currentZoom) - 1);
			if (maxX < 0)
				maxX = 0;
			if (maxX >= (1 << currentZoom))
				maxX = ((1 << currentZoom) - 1);
			int minY = (int) Math.floor((1 - Math
					.log(Math.tan(Math.toRadians(bbox.maxLatitude)) + 1 / Math.cos(Math.toRadians(bbox.maxLatitude)))
					/ Math.PI) / 2 * (1 << currentZoom));
			int maxY = (int) Math.floor((1 - Math
					.log(Math.tan(Math.toRadians(bbox.minLatitude)) + 1 / Math.cos(Math.toRadians(bbox.minLatitude)))
					/ Math.PI) / 2 * (1 << currentZoom));
			if (minY < 0)
				minY = 0;
			if (minY >= (1 << currentZoom))
				minY = ((1 << currentZoom) - 1);
			if (maxY < 0)
				maxY = 0;
			if (maxY >= (1 << currentZoom))
				maxY = ((1 << currentZoom) - 1);
			for (int x = minX; x <= maxX; x++) {
				boolean newTiles = false;
				for (int y = minY; y <= maxY; y++) {
					File outputFile = new File(pregeneratedPath, currentZoom + "/" + x + "/" + y + ".png");
					TileWriter writer = mapsforgeManager.getTileWriter(currentZoom + ";" + x + ";" + y, outputFile,
							sender, tileExecutor, labelExecutor);

					executor.submit(writer);
					newTiles = true;
				}
				if (newTiles)
					System.out.println(
							"[" + Instant.now() + "] " + "Queued " + Integer.toString(queue.size()) + " Tiles");
			}
		}

		executor.shutdown();
		sender.submitTilesDynamic(executor, pregeneratedPath);
		System.out.println("[" + Instant.now() + "] Finished");
	}

	private void generateMapFiles() throws IOException, InterruptedException {
		// First we need an Array of all .osm.pbf Files

		File[] files = this.splittedPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".osm.pbf");
			}
		});

		// Find out how many processes to spawn (each map file generator will consume
		// around 2GB ram
		int memoryGB = Math.min(this.maxMemoryGB, 12);

		int threadCount = Math.min(this.maxThreads, (int) Math.floor(this.maxMemoryGB / memoryGB));

		// Export Memory and Tempdir Options for Osmosis
		FileUtils.writeStringToFile(osmosisOptions, "export JAVACMD_OPTIONS=\"-Xmx" + memoryGB + "G -Djava.io.tmpdir="
				+ this.osmosisTemp.getAbsolutePath() + "\"");

		System.out.println("Generating mapfiles with " + threadCount + " processes.");

		LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
		ThreadPoolExecutor executor = new ThreadPoolExecutor(threadCount, threadCount, 5, TimeUnit.SECONDS, queue);
		File areasList = new File(this.splittedPath, "areas.list");
		@SuppressWarnings("unchecked")
		List<String> areas = (List<String>) FileUtils.readLines(areasList);

		Pattern p = Pattern.compile(
				"^\\d+:\\s([-]{0,1}[\\d\\.]+),([-]{0,1}[\\d\\.]+)\\sto\\s([-]{0,1}[\\d\\.]+),([-]{0,1}[\\d\\.]+).*$");
		for (File file : files) {
			// Filename of the Mapfile
			File mapFile = new File(file.getParentFile(),
					file.getName().substring(0, file.getName().indexOf(".")) + ".map");

			// Find out the Boundingbox for this mapfile
			String basename = FilenameUtils.getBaseName(mapFile.getName());
			String bboxString = null;
			for (String line : areas) {
				if (line.startsWith(basename)) {
					bboxString = line;
					break;
				}
			}
			if (bboxString != null) {
				Matcher m = p.matcher(bboxString);
				if (m.matches()) {
					BoundingBox bbox = new BoundingBox(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)),
							Double.parseDouble(m.group(3)), Double.parseDouble(m.group(4)));
					executor.submit(new MapFileGenerator(this.osmosisPath, file, mapFile, bbox));
				}
			}
		}
		executor.shutdown();
		int remaining = queue.size();
		while (!executor.isTerminated()) {
			if (queue.size() != remaining) {
				System.out.println(queue.size() + " Mapfiles remaining.");
				remaining = queue.size();
			}
			executor.awaitTermination(5, TimeUnit.SECONDS);
		}
		File[] mapFiles = this.splittedPath.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".map") || name.equals("areas.list");
			}
		});

		sender.submitNewMapfiles(mapFiles);

	}

	private void splitShapes(File sourceShape, File targetName, BoundingBox bbox)
			throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder();
		pb.command("ogr2ogr", "-overwrite", "-progress", "-skipfailures", "-clipsrc",
				Double.toString(bbox.minLongitude),
				Double.toString(bbox.minLatitude),
				Double.toString(bbox.maxLongitude),
				Double.toString(bbox.maxLatitude),
				targetName.getAbsolutePath(), sourceShape.getAbsolutePath());
		pb.inheritIO();
		Process process = pb.start();
		process.waitFor();
		if (process.exitValue() != 0)
			throw new IOException();
	}

	private BoundingBox calculateBBox() throws IOException {
		// Read the bboxes from the Shape Files
		BoundingBox bbox = null;
		String shapeFile = FileUtils.readFileToString(this.polyFile);
		Pattern p = Pattern.compile("\n\\d+\n(.*?)END", Pattern.DOTALL);
		Pattern p2 = Pattern.compile("^\\s*([^\\s]+)\\s+([^\\s]+)$", Pattern.MULTILINE);
		Matcher m = p.matcher(shapeFile);
		double minLong = 0, minLat = 0, maxLong = 0, maxLat = 0;
		while (m.find()) {

			Matcher m2 = p2.matcher(m.group(1));
			while (m2.find()) {
				double tmpLong = Double.parseDouble(m2.group(1));
				double tmpLat = Double.parseDouble(m2.group(2));
				if (minLong == 0 || tmpLong < minLong) {
					minLong = tmpLong;
				}
				if (maxLong == 0 || tmpLong > maxLong) {
					maxLong = tmpLong;
				}
				if (minLat == 0 || tmpLat < minLat) {
					minLat = tmpLat;
				}
				if (maxLat == 0 || tmpLat > maxLat) {
					maxLat = tmpLat;
				}

			}

			// Round the values to add a little bit of extra space
			minLat = Math.floor(minLat * 10) / 10.0;
			minLong = Math.floor(minLong * 10) / 10.0;
			maxLat = Math.ceil(maxLat * 10) / 10.0;
			maxLong = Math.ceil(maxLong * 10) / 10.0;
			bbox = new BoundingBox(minLat, minLong, maxLat, maxLong);
		}
		return bbox;
	}

	private void splitOsmFile() throws IOException, InterruptedException {
		File osmFile = new File(this.splittedPath, "osm-data.osm.pbf");

		ProcessBuilder pb = new ProcessBuilder();
		pb.inheritIO();
		ArrayList<String> command = new ArrayList<>();

		command.add("java");
		command.add("-Xmx" + this.maxMemoryGB + "G");
		command.add("-jar");
		command.add(this.splitterPath.getAbsolutePath());
		command.add("--mapid=1");
		command.add("--output-dir=" + this.splittedPath.getAbsolutePath());
		command.add("--wanted-admin-level=0");
		command.add(osmFile.getAbsolutePath());

		pb.command(command);
		Process p = pb.start();
		p.waitFor();
		if (p.exitValue() != 0)
			throw new IOException();
		FileUtils.forceDelete(osmFile);

		// Now adjust the areas.list
		File areasList = new File(this.splittedPath, "areas.list");
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(areasList), Charset.defaultCharset()));) {
			String line = "";
			Pattern pattern = Pattern.compile("^(\\d+)");
			Pattern pattern2 = Pattern
					.compile("([-]{0,1}[\\d\\.]+,[-]{0,1}[\\d\\.]+\\sto\\s[-]{0,1}[\\d\\.]+,[-]{0,1}[\\d\\.]+)");
			StringBuilder newAreas = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				Matcher matcher2 = pattern2.matcher(line);
				if (matcher.find()) {
					newAreas.append(matcher.group(1) + ": ");
				} else if (matcher2.find()) {
					newAreas.append(matcher2.group(1) + "\n");
				}
			}

			FileUtils.writeStringToFile(areasList, newAreas.toString());
		}
		File[] oldFiles = this.splittedPath.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return !name.endsWith(".osm.pbf") && !name.endsWith("areas.list");
			}
		});
		for (File file : oldFiles) {
			FileUtils.forceDelete(file);
		}
	}

	private void downloadOsmFile() {
		while (true) {
			try (InputStream in = this.dataUrl.openStream();
					OutputStream out = new FileOutputStream(this.osmFile, false)) {
				IOUtils.copy(in, out);
				break;
			} catch (IOException e) {
				e.printStackTrace();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		while (true) {
			try (InputStream in = this.polyUrl.openStream();
					OutputStream out = new FileOutputStream(this.polyFile, false)) {
				IOUtils.copy(in, out);
				break;
			} catch (IOException e) {
				e.printStackTrace();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public File getDataDir() {
		return dataDir;
	}

	public File getSplittedPath() {
		return splittedPath;
	}

	public File getPregeneratedPath() {
		return pregeneratedPath;
	}

	public File getOsmosisPath() {
		return osmosisPath;
	}

	public File getOsmosisTemp() {
		return osmosisTemp;
	}

	public File getLandShapeFile() {
		return landShapeFile;
	}

	public File getShape2osmPathLand() {
		return shape2osmPathLand;
	}

	public File getOsmosisOptions() {
		return osmosisOptions;
	}

	public File getLockFile() {
		return lockFile;
	}

	public int getMaxThreads() {
		return maxThreads;
	}

	public File getOsmFile() {
		return osmFile;
	}

	public File getPolyFile() {
		return polyFile;
	}

	public URL getDataUrl() {
		return dataUrl;
	}

	public UpdateSender getSender() {
		return sender;
	}

	public MapsforgeManager getMapsforgeManager() {
		return mapsforgeManager;
	}

	public int getMaxMemoryGB() {
		return maxMemoryGB;
	}

	public URL getPolyUrl() {
		return polyUrl;
	}

	public BoundingBox getBbox() {
		return bbox;
	}

}
