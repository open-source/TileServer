package de.metager.updater;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.mapsforge.core.model.BoundingBox;

public class Shape2Osm implements Runnable {

	private File sourceShape;
	private BoundingBox bbox;
	private Updater updater;

	public Shape2Osm(File sourceShape, BoundingBox bbox, Updater updater) {
		this.sourceShape = sourceShape;
		this.bbox = bbox;
		this.updater = updater;
	}

	@Override
	public void run() {
		while (true) {
			try {
				// Transform ShapeFile to OSM File
				ProcessBuilder pb = new ProcessBuilder();
				pb.command("python", updater.getShape2osmPathLand().getAbsolutePath(), "-l",
						new File(sourceShape.getParentFile(),
								FilenameUtils.getBaseName(sourceShape.getName()) + ".land").getAbsolutePath(),
						sourceShape.getAbsolutePath());
				// TODO make portable to Windows
				pb.redirectOutput(new File("/dev/null"));
				Process process = pb.start();
				process.waitFor();
				if (process.exitValue() != 0)
					throw new IOException();
				else {
					// Delete the Shapefile
					String filename = FilenameUtils.getBaseName(sourceShape.getName());
					File[] oldFiles = sourceShape.getParentFile().listFiles(new FilenameFilter() {

						@Override
						public boolean accept(File dir, String name) {
							return name.equals(filename + ".shp") || name.equals(filename + ".dbf")
									|| name.equals(filename + ".prj") || name.equals(filename + ".shx");
						}
					});
					for (File oldFile : oldFiles) {
						FileUtils.forceDelete(oldFile);
					}
				}
				// Create the water.osm
				File waterFile = new File(sourceShape.getParentFile(),
						FilenameUtils.getBaseName(sourceShape.getName()) + ".water.osm");
				String content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<osm version=\"0.6\">\n"
						+ "    <node timestamp=\"1969-12-31T23:59:59Z\" changeset=\"-1\" id=\"32951459320\" version=\"1\" lon=\""
						+ bbox.minLongitude + "\" lat=\""
						+ bbox.minLatitude + "\" />\n"
						+ "    <node timestamp=\"1969-12-31T23:59:59Z\" changeset=\"-1\" id=\"32951459321\" version=\"1\" lon=\""
						+ bbox.minLongitude + "\" lat=\""
						+ bbox.maxLatitude + "\" />\n"
						+ "    <node timestamp=\"1969-12-31T23:59:59Z\" changeset=\"-1\" id=\"32951459322\" version=\"1\" lon=\""
						+ bbox.maxLongitude + "\" lat=\""
						+ bbox.maxLatitude + "\" />\n"
						+ "    <node timestamp=\"1969-12-31T23:59:59Z\" changeset=\"-1\" id=\"32951459323\" version=\"1\" lon=\""
						+ bbox.maxLongitude + "\" lat=\""
						+ bbox.minLatitude + "\" />\n"
						+ "    <way timestamp=\"1969-12-31T23:59:59Z\" changeset=\"-1\" id=\"32951623372\" version=\"1\">\n"
						+ "        <nd ref=\"32951459320\" />\n" + "        <nd ref=\"32951459321\" />\n"
						+ "        <nd ref=\"32951459322\" />\n" + "        <nd ref=\"32951459323\" />\n"
						+ "        <nd ref=\"32951459320\" />\n" + "        <tag k=\"area\" v=\"yes\" />\n"
						+ "        <tag k=\"layer\" v=\"-5\" />\n" + "        <tag k=\"natural\" v=\"sea\" />\n"
						+ "    </way>\n" + "</osm>";

				FileUtils.writeStringToFile(waterFile, content);
				break;
			} catch (IOException | InterruptedException e) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
