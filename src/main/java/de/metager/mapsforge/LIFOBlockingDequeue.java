package de.metager.mapsforge;

import java.util.concurrent.LinkedBlockingDeque;

public class LIFOBlockingDequeue<T> extends LinkedBlockingDeque<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -231146914369457118L;
	@Override
	public boolean offer(T t) {
		if(this.size() < 50)
			return super.offerFirst(t);
		else
			return super.offer(t);
	}
	@Override
	public T remove() {
		return super.removeFirst();
	}
}
