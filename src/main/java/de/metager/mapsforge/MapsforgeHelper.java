package de.metager.mapsforge;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.Point;
import org.mapsforge.core.model.Rectangle;
import org.mapsforge.map.datastore.MultiMapDataStore;
import org.mapsforge.map.datastore.MultiMapDataStore.DataPolicy;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.reader.header.MapFileException;

public class MapsforgeHelper {

	public static MultiMapDataStore getMultiMapDataStore(File mapFileDir) {
		MultiMapDataStore mf = new MultiMapDataStore(DataPolicy.RETURN_ALL);
		File[] mapFileDirs = mapFileDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				return pathname.isDirectory();
			}
		});
		if(mapFileDirs.length > 0) {
			Arrays.sort(mapFileDirs);
			File[] mapFiles = mapFileDirs[0].listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.isFile() && pathname.getName().toLowerCase().endsWith(".map");
				}
			});
			for (File mapFile : mapFiles) {
				try {
					mf.addMapDataStore(new MapFile(mapFile), false, false);
				} catch (MapFileException e) {
					System.err.println("Couldn't load " + mapFile.getAbsolutePath());
				}
			}
		}else {
			File[] mapFiles = mapFileDir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					return pathname.isFile() && pathname.getName().endsWith(".map");
				}
			});
			for (File mapFile : mapFiles) {
				try {
					mf.addMapDataStore(new MapFile(mapFile), false, false);
				} catch (MapFileException e) {
					System.err.println("Couldn't load " + mapFile.getAbsolutePath());
				}
			}
		}
		return mf;
	}

	public static BoundingBox generateBoundingBox(File mapFilePath) {
		File[] mapFiles = mapFilePath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().toLowerCase().endsWith(".map")
						&& !pathname.getName().startsWith("world");
			}
		});
		if (mapFiles.length <= 0) {
			return null;
		} else {
			MultiMapDataStore mf = new MultiMapDataStore(DataPolicy.RETURN_ALL);
			for (File mapFile : mapFiles) {
				try {
					mf.addMapDataStore(new MapFile(mapFile), false, false);
				} catch (MapFileException e) {
					System.err.println("Couldn't load " + mapFile.getAbsolutePath());
				}
			}
			BoundingBox result = mf.boundingBox();
			mf.close();
			return result;
		}
	}
	public static MultiMapDataStore getMapDataStore(File mapFilePath, Rectangle rect) throws FileNotFoundException, IOException {
		ArrayList<File> mapFiles = getMapFilesForBoundingBox(mapFilePath, rect);
		MultiMapDataStore store = new MultiMapDataStore(DataPolicy.DEDUPLICATE);
		for(File mapFile : mapFiles) {
			store.addMapDataStore(new MapFile(mapFile), false, false);
		}
		return store;
	}

	public static ArrayList<File> getMapFilesForBoundingBox(File mapFilePath, Rectangle rect)
			throws FileNotFoundException, IOException {
		File[] mapFileDirs = mapFilePath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				return pathname.isDirectory();
			}
		});
		Arrays.sort(mapFileDirs);
		mapFilePath = mapFileDirs[0];
		String areas = IOUtils.toString(new FileInputStream(new File(mapFilePath, "areas.list")));

		Pattern pattern = Pattern.compile(
				"^(\\d+):\\s([-]{0,1}[\\d\\.]+),([-]{0,1}[\\d\\.]+)\\sto\\s([-]{0,1}[\\d\\.]+),([-]{0,1}[\\d\\.]+)$", Pattern.MULTILINE);

		Matcher m = pattern.matcher(areas);

		ArrayList<File> mapfiles = new ArrayList<>();
		while (m.find()) {
			String areaName = m.group(1);
			Rectangle aRect = new Rectangle(Double.parseDouble(m.group(3)), Double.parseDouble(m.group(2)),
					Double.parseDouble(m.group(5)), Double.parseDouble(m.group(4)));

			// If the two rectangles are intersecting or
			// if one rectangle contains the other we will add it to the download
			if (aRect.intersects(rect)
					|| (aRect.contains(new Point(rect.left, rect.top))
							&& aRect.contains(new Point(rect.left, rect.bottom))
							&& aRect.contains(new Point(rect.right, rect.top))
							&& aRect.contains(new Point(rect.right, rect.bottom)))
					|| (rect.contains(new Point(aRect.left, aRect.top))
							&& rect.contains(new Point(aRect.left, aRect.bottom))
							&& rect.contains(new Point(aRect.right, aRect.top))
							&& rect.contains(new Point(aRect.right, aRect.bottom)))) {
				mapfiles.add(new File(mapFilePath, areaName + ".map"));
			}

		}
		

		return mapfiles;
	}

	public static ArrayList<File> getTilesForBoundingBox(Rectangle rect, File tilePath) {
		File[] tilePaths = tilePath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				return pathname.isDirectory();
			}
		});
		Arrays.sort(tilePaths);
		tilePath = tilePaths[0];
		ArrayList<File> tiles = new ArrayList<>();
		
		// We will calculate all tiles for zoom 0-13 which lie within the given rect
		for(int z = 0; z <= 13; z++) {
			// Calculate min tile and max tile for the rect on this zoom
			int xtileMin = (int)Math.floor( (rect.left + 180) / 360 * (1<<z) );
			int ytileMin = (int)Math.floor( (1 - Math.log(Math.tan(Math.toRadians(rect.bottom)) + 1 / Math.cos(Math.toRadians(rect.bottom))) / Math.PI) / 2 * (1<<z) );
			
			int xtileMax = (int)Math.floor( (rect.right + 180) / 360 * (1<<z) );
			int ytileMax = (int)Math.floor( (1 - Math.log(Math.tan(Math.toRadians(rect.top)) + 1 / Math.cos(Math.toRadians(rect.top))) / Math.PI) / 2 * (1<<z) );
			
			for(int x = xtileMin; x <= xtileMax; x++) {
				for(int y = ytileMin; y <= ytileMax; y++) {
					File tileFile = new File(tilePath, "/" + z + "/" + x + "/" + y + ".png");
					tiles.add(tileFile);
				}
			}
			
		}
		
		
		return tiles;
	}

	public static Long getDataAge(File mapFilePath) {
		File[] mapFileDirs = mapFilePath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				return pathname.isDirectory();
			}
		});
		Arrays.sort(mapFileDirs);
		if(mapFileDirs.length > 0) {
			mapFilePath = mapFileDirs[0];
			return Long.parseLong(mapFilePath.getName());
		}else {
			return null;
		}
	}
}
