package de.metager.mapsforge;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.mapsforge.core.graphics.GraphicFactory;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.datastore.MultiMapDataStore;
import org.mapsforge.map.layer.cache.FileSystemTileCache;
import org.mapsforge.map.layer.labels.MapDataStoreLabelStore;
import org.mapsforge.map.layer.renderer.DatabaseRenderer;
import org.mapsforge.map.model.DisplayModel;
import org.mapsforge.map.rendertheme.ExternalRenderTheme;
import org.mapsforge.map.rendertheme.rule.RenderThemeFuture;
import de.metager.tileserver.AssetsDownloader;
import de.metager.tileserver.FileDownloader;
import de.metager.tileserver.FileLister;
import de.metager.tileserver.Main;
import de.metager.tileserver.TileRenderer;
import de.metager.tileserver.TileWriter;
import de.metager.updater.UpdateSender;

public class MapsforgeManager {

	private final File pathPrefix;
	private File prerenderedTilePath;
	private File tileCacheDBPath = null;
	private Connection con;
	private final File tileCachePath;
	private final File mapFilePath;
	private final File renderThemePath;
	private final int numberProcesses = Runtime.getRuntime().availableProcessors();
	private final GraphicFactory GRAPHIC_FACTORY = AwtGraphicFactory.INSTANCE;
	private MultiMapDataStore multiMapDataStore;
	private ExternalRenderTheme xmlRenderTheme;
	private DisplayModel displayModel;
	private RenderThemeFuture renderThemeFuture;
	private FileSystemTileCache tileCache;
	private DatabaseRenderer databaseRenderer;
	private ThreadPoolExecutor lowResExecutor;
	private ThreadPoolExecutor highResExecutor;
	private ThreadPoolExecutor downloadExecutor;
	private ReadWriteLock updateLock;
	private ReentrantReadWriteLock mapFileLock;
	private MapDataStoreLabelStore labelStore;
	private ExecutorService executorHelper;
	private Long dataAge;
	private Pattern tilePattern = Pattern.compile("(\\d+);(\\d+);(\\d+)");

	public MapsforgeManager(ReadWriteLock updateLock, File pathPrefix) {
		this.pathPrefix = pathPrefix;
		prerenderedTilePath = new File(pathPrefix, "prerendered");
		tileCacheDBPath = new File(pathPrefix, "data.sqlite");
		tileCachePath = new File(pathPrefix, "tile-cache");
		mapFilePath = new File(pathPrefix, "mapfiles");
		renderThemePath = new File(pathPrefix, "render-theme");
		this.updateLock = updateLock;
		this.mapFileLock = new ReentrantReadWriteLock(true);

		initialize();

	}

	public MapsforgeManager(File pathPrefix, File prerenderedTilePath, File tileCachePath, File mapFilePath,
			File renderThemePath) {
		this.pathPrefix = pathPrefix;
		this.prerenderedTilePath = prerenderedTilePath;
		this.tileCachePath = tileCachePath;
		this.mapFilePath = mapFilePath;
		this.renderThemePath = renderThemePath;

		initialize();
	}

	private void initialize() {
		try {
			// Create Prerendered Tiles if it does not exist
			prerenderedTilePath.mkdirs();
			// Initialize database if needed
			if(tileCacheDBPath != null) {
				if(!tileCacheDBPath.exists()) {
					FileUtils.touch(tileCacheDBPath);
				}
				try {
					Class.forName("org.sqlite.JDBC");
					con = DriverManager.getConnection("jdbc:sqlite:" + tileCacheDBPath.getAbsolutePath());
				} catch (SQLException | ClassNotFoundException e) {
					System.err.println("Couldn't initialize Database");
					e.printStackTrace();
					System.exit(-1);
				}
				// Create the tile table if it does not exist
				try(
					Statement stmt = con.createStatement();
				){
					stmt.executeUpdate(
							"CREATE TABLE IF NOT EXISTS tiles ("
							+ "`x` INTEGER NOT NULL,"
							+ "`y` INTEGER NOT NULL,"
							+ "`z` INTEGER NOT NULL,"
							+ "`image` BLOB NOT NULL,"
							+ "`lastModified` INTEGER NOT NULL,"
							+ "primary key (`x`,`y`,`z`)"
							+ ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
			// Create Mapfile Path if it does not exist
			mapFilePath.mkdirs();
			// Create Render Theme Path if it does not exist
			renderThemePath.mkdirs();
			// Create Tile Cache Path if it does not exist
			tileCachePath.mkdirs();

			// Cancel if we are not allowed to write or read the necessary Directories
			if (!pathPrefix.canWrite() || !pathPrefix.canWrite()) {
				throw new IOException("Cannot read/write to " + prerenderedTilePath.getParentFile().getAbsolutePath());
			}

			// Check for existence of Rendertheme File
			this.askForRenderTheme();
			// Check for existence of the assets Folder
			this.askForAssetsFolder();

			// Create Multimapdatastore
			this.dataAge = MapsforgeHelper.getDataAge(mapFilePath);
			this.multiMapDataStore = MapsforgeHelper.getMultiMapDataStore(mapFilePath);

			this.xmlRenderTheme = new ExternalRenderTheme(new File(renderThemePath, "metager.xml"));

			// Create Display Model and Render Theme
			this.displayModel = new DisplayModel();
			displayModel.setFixedTileSize(256);
			this.renderThemeFuture = new RenderThemeFuture(GRAPHIC_FACTORY, xmlRenderTheme, displayModel);

			renderThemeFuture.run();
			try {
				renderThemeFuture.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// new Thread(renderThemeFuture).start();

			this.tileCache = new FileSystemTileCache(1024, tileCachePath, GRAPHIC_FACTORY, true);
			// this.labelCache = new TileBasedLabelStore(4096);
			this.databaseRenderer = new DatabaseRenderer(this.multiMapDataStore, GRAPHIC_FACTORY, tileCache, null,
					false, false, null);
			this.labelStore = new MapDataStoreLabelStore(multiMapDataStore, renderThemeFuture, 1f, displayModel,
					GRAPHIC_FACTORY);

			LIFOBlockingDequeue<Runnable> lowResQueue = new LIFOBlockingDequeue<>();
			this.lowResExecutor = new ThreadPoolExecutor(1, this.numberProcesses * 2, 10, TimeUnit.SECONDS,
					lowResQueue);
			LIFOBlockingDequeue<Runnable> highResQueue = new LIFOBlockingDequeue<>();
			this.highResExecutor = new ThreadPoolExecutor(1, this.numberProcesses * 2, 10, TimeUnit.SECONDS,
					highResQueue);
			LIFOBlockingDequeue<Runnable> downloadQueue = new LIFOBlockingDequeue<>();
			this.downloadExecutor = new ThreadPoolExecutor(1, this.numberProcesses * 2, 10, TimeUnit.SECONDS,
					downloadQueue);
			this.executorHelper = Executors.newCachedThreadPool();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public void updateMapFiles(File newMapFiles) throws IOException {
		this.multiMapDataStore.close();
		this.multiMapDataStore = MapsforgeHelper.getMultiMapDataStore(mapFilePath);
		this.databaseRenderer = new DatabaseRenderer(this.multiMapDataStore, GRAPHIC_FACTORY, tileCache, null, true,
				false, null);
		this.labelStore = new MapDataStoreLabelStore(multiMapDataStore, renderThemeFuture, 1f, displayModel,
				GRAPHIC_FACTORY);
	}

	private void askForAssetsFolder() throws IOException {
		if (new File(renderThemePath, "assets").exists())
			return;
		// Copy the Assets Folder from the Resources
		try (ZipInputStream zis = new ZipInputStream(Main.class.getResourceAsStream("/assets.zip"));) {
			ZipEntry entry;
			while ((entry = zis.getNextEntry()) != null) {
				File newFile = new File(renderThemePath, entry.getName());
				if (!entry.isDirectory()) {
					newFile.getParentFile().mkdirs();
					try (FileOutputStream out = new FileOutputStream(newFile)) {
						IOUtils.copy(zis, out);
					}
				} else {
					newFile.mkdirs();
				}
			}
		}
	}

	private void askForRenderTheme() throws FileNotFoundException, IOException {
		if (new File(renderThemePath, "metager.xml").exists())
			return;
		try (InputStream in = Main.class.getResourceAsStream("/metager.xml");
				OutputStream out = new FileOutputStream(new File(renderThemePath, "metager.xml"))) {
			IOUtils.copy(in, out);
		}
	}

	public void renderTileToClient(Socket clientSocket, String command) {
		
		Matcher m = tilePattern.matcher(command);
		if (m.find()) {
			int z = Integer.parseInt(m.group(1));
			if(z <=13 )
				lowResExecutor.submit(new TileRenderer(this, clientSocket, command, executorHelper));
			else
				highResExecutor.submit(new TileRenderer(this, clientSocket, command, executorHelper));
		}else {
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void listFilesToClient(Socket clientSocket, String arguments, ReadWriteLock updateLock) {
		downloadExecutor.submit(new FileLister(clientSocket, arguments, mapFilePath, prerenderedTilePath, updateLock));
	}

	public void downloadFilesToClient(Socket clientSocket, String arguments, ReadWriteLock updateLock) {
		downloadExecutor.submit(new FileDownloader(clientSocket, arguments, mapFilePath, prerenderedTilePath, updateLock));
	}

	public void downloadAssetsToClient(Socket clientSocket) {
		downloadExecutor.submit(new AssetsDownloader(clientSocket, new File(this.renderThemePath, "metager.xml")));
	}

	public TileWriter getTileWriter(String tile, File outputFile, UpdateSender sender, ExecutorService tileExecutor,
			ExecutorService labelExecutor) {
		return new TileWriter(sender, tile, outputFile, this, tileExecutor, labelExecutor);
	}

	public File getPrerenderedTilePath() {
		if (prerenderedTilePath.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		}).length == 0) {
			File newCache = new File(prerenderedTilePath, Long.toString(System.currentTimeMillis()));
			newCache.mkdirs();
		}
		return prerenderedTilePath;
	}

	public File getMapFilePath() {
		return mapFilePath;
	}

	public File getPathPrefix() {
		return pathPrefix;
	}

	public File getTileCachePath() {
		return tileCachePath;
	}

	public File getRenderThemePath() {
		return renderThemePath;
	}

	public int getNumberProcesses() {
		return numberProcesses;
	}

	public GraphicFactory getGRAPHIC_FACTORY() {
		return GRAPHIC_FACTORY;
	}

	public MultiMapDataStore getMultiMapDataStore() {
		return multiMapDataStore;
	}

	public ExternalRenderTheme getXmlRenderTheme() {
		return xmlRenderTheme;
	}

	public DisplayModel getDisplayModel() {
		return displayModel;
	}

	public RenderThemeFuture getRenderThemeFuture() {
		return renderThemeFuture;
	}

	public FileSystemTileCache getTileCache() {
		return tileCache;
	}

	public DatabaseRenderer getDatabaseRenderer() {
		return databaseRenderer;
	}

	public ReadWriteLock getUpdateLock() {
		return updateLock;
	}

	public ReentrantReadWriteLock getMapFileLock() {
		return mapFileLock;
	}

	public MapDataStoreLabelStore getLabelStore() {
		return labelStore;
	}

	public File getTileCacheDBPath() {
		return tileCacheDBPath;
	}

	public Connection getCon() {
		return con;
	}

	public ExecutorService getExecutorHelper() {
		return executorHelper;
	}

	public Long getDataAge() {
		return dataAge;
	}

}
